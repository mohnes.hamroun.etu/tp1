"use strict";

//let html ='<a href='+url+'>'+url+'</a>'

/*let html = `<a href="${url}">
<img src="${url}"/>
<section>${name}</section>
</a>`*/
var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'images/regina.jpg'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'images/napolitaine.jpg'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'images/spicy.jpg'
}];
/* G.1.1
data.sort(function(a,b){
    if(a.name<b.name){
        return -1;
    }else if(a.name>b.name){
        return 1;
    }else{
        return 0;
    }
});*/

/*G.1.3
data.sort(function(a,b){
    if(a.price_small<b.price_small){return -1}
    else if(a.price_small>b.price_small){return 1}
    else{
        if(a.price_large<b.price_large){return -1}
        else if(a.price_large>b.price_large){return 1}
        else{return 0};
    }
});*/

/*G.2.1
data = data.filter(pizza => pizza.base=="tomate")*/

/*G.2.2
data = data.filter(pizza => pizza.price_small<=6)*/

/*G.2.3
data = data.filter(({name}) => name.split('i').length-1==2);*/

var html = "";
/*data.forEach(({name,base,price_small,price_large,image}) => {
    html+=`<article class="pizzaThumbnail">
	<a href="${image}">
		<img src="${image}" />
		<section>
			<h4>${name}</h4>
			<ul>
				<li>Prix petit format : ${price_small} €</li>
				<li>Prix grand format : ${price_large} €</li>
			</ul>
		</section>
	</a>
    </article>`

    
})*/

data = data.map(function (_ref) {
  var name = _ref.name,
      price_small = _ref.price_small,
      price_large = _ref.price_large,
      image = _ref.image;
  html += "<article class=\"pizzaThumbnail\">\n\t<a href=\"".concat(image, "\">\n\t\t<img src=\"").concat(image, "\" />\n\t\t<section>\n\t\t\t<h4>").concat(name, "</h4>\n\t\t\t<ul>\n\t\t\t\t<li>Prix petit format : ").concat(price_small, " \u20AC</li>\n\t\t\t\t<li>Prix grand format : ").concat(price_large, " \u20AC</li>\n\t\t\t</ul>\n\t\t</section>\n\t</a>\n    </article>");
});
console.log("PizzaLand");
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map